// Declare uma variável contendo uma string
var nome = "Cristhian";
// Declare uma variável contendo um número dentro de uma string
var numero = "2023";
// Declare uma variável com a sua idade
var idade = 26;
// Declare duas variáveis, uma com seu nome
// e outra com seu sobrenome e some as mesmas
var sobrenome = "Almeida";
var nomeCompleto = `${nome}  ${sobrenome}`;
// Coloque a seguinte frase em uma variável: It's time
var frase = "It's time";
// Verifique o tipo da variável que contém o seu nome
var verificarTipoNome = typeof nome

console.log(nome, numero, idade, nomeCompleto, frase, verificarTipoNome);

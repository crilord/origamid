// Qual o resultado da seguinte expressão?
var total = 10 + (5 * 2) / 2 + 20;
// total = 35

// Crie duas expressões que retornem NaN
var expressao1 = "a 20" / 2;
var expressao2 = "b 30" * 3;

console.log(expressao1, expressao2)
// Somar a string '200' com o número 50 e retornar 250
var soma = +'200' + 50;
console.log(soma)
// Incremente o número 5 e retorne o seu valor incrementado
var x = 5;
console.log(++x)
// Como dividir o peso por 2?
var numero = 80 / 2;
var unidade = "kg";
var peso = numero + unidade; // '40kg'

console.log(peso)
